<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\CourseCategory;

class CourseTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $courses = [
        	['course_name' => 'Bachelor of Science in Information Technology- Network Security'],
        	['course_name' => 'Bachelor of Science in Information Technology- Web Technology'],
        	['course_name' => 'Bachelor of Science in Computer Science- Digital Arts and Animation'],
        	['course_name' => 'Bachelor of Science in Computer Science- Mobile Technology'],
        	['course_name' => 'Bachelor of Science in Computer Science- Embedded Application'],
        	['course_name' => 'Bachelor of Science in Data Analytics'],
        	['course_name' => 'Associate in Computer Technology'],

        
        ];
        CourseCategory::insert($courses);
    }
}
