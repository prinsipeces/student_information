<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
})->name('login');

Route::post('/login', 'App\Http\Controllers\LoginController@store');

Route::middleware('auth')
	->group(function() {
        //logout
		Route::get('/logout', 'App\Http\Controllers\LoginController@logout');
        //change password
        Route::get('/changepassword','App\Http\Controllers\LoginController@showChangePasswordForm');
        Route::post('/changePassword','App\Http\Controllers\LoginController@changePassword')->name('changePassword');

        //admin
		Route::get('/admin', 'App\Http\Controllers\AdminController@index');
        //add student
        Route::get('/admin/create', 'App\Http\Controllers\AdminController@create');
        //store student
        Route::post('/admin/store', 'App\Http\Controllers\AdminController@store');
         //edit student
        Route::get('/admin/{student}/edit', 'App\Http\Controllers\AdminController@edit');
        //update student
        Route::post('/admin/{student}/update', 'App\Http\Controllers\AdminController@update');
        //delete student
        Route::get('/admin/{student}/delete', 'App\Http\Controllers\AdminController@delete');


        //show users
        Route::get('/admin/showusers', 'App\Http\Controllers\AdminController@showUser');
        //add user
        Route::get('/admin/adduser', 'App\Http\Controllers\AdminController@addUser');
        //store user
        Route::post('/admin/storeUser', 'App\Http\Controllers\AdminController@storeUser');
         //edit user
        Route::get('/admin/showusers/{user}/edituser', 'App\Http\Controllers\AdminController@editUser');
        //update user
        Route::post('/admin/showusers/{user}/updateuser', 'App\Http\Controllers\AdminController@updateUser');
        //delete user
        Route::get('/admin/showusers/{user}/deleteuser', 'App\Http\Controllers\AdminController@deleteUser');



	});