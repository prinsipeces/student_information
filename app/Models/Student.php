<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function course_categories() {
    	return $this->belongsTo(CourseCategory::class, 'course_id');
    }


    public function getFullNameAttribute()
    {
        return $this->first_name.' '.$this->middle_name.'-'.$this->last_name;
    }
    
}
