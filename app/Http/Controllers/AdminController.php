<?php

namespace App\Http\Controllers;

use App\Models\CourseCategory;
use App\Models\Student;
use App\Models\User;
use Hash;
use Illuminate\Http\Request;


class AdminController extends Controller
{
    //
    public function index()
    {
    	$students = Student::all();
    	return view('admin.index', compact('students'));
    }

    //add student
    public function create()
    {
    	$course_categories = CourseCategory::all();
    	return view('admin.create', compact('course_categories'));
    }

    //store student
    public function store()
    {
    	// validate the form
    	request()->validate([
    		'first_name' => 'required',
    		'middle_name' => 'required',
    		'last_name' => 'required',
    		'gender' => 'required',
    		'email_address' => 'required',
    		'phone_number' => 'required',
    		'student_id_number' => 'required',
    		'course_id' => 'required',
    	]);
    	// store the model
    	$students = Student::create(request()->only([
    		'first_name', 'middle_name', 'last_name', 'gender', 'email_address', 'phone_number', 'student_id_number', 'course_id'
    	]));
    	// redirect to equipment index
    	return redirect('/admin')->with("success","Added!");
    }

    //edit student
    public function edit(Student $student)
    {
        $course_categories = CourseCategory::all();
        return view('admin.edit', compact('student', 'course_categories'));
    }

    //update student
    public function update(Student $student)
    {
        request()->validate([
            'first_name' => 'required',
            'middle_name' => 'required',
            'last_name' => 'required',
            'gender' => 'required',
            'email_address' => 'required',
            'phone_number' => 'required',
            'student_id_number' => 'required',
            'course_id' => 'required',
        ]);
        $student->update(request()->only([
            'first_name', 'middle_name', 'last_name', 'gender', 'email_address', 'phone_number', 'student_id_number', 'course_id'
        ]));

        return redirect('/admin')->with("success","Updated!");
    }

    //delete student
    public function delete(Student $student)
    {
        $student->delete();

        return redirect()->back()->with("error","Deleted!");
    }


    //show users
    public function showUser()
    {
        $users = User::all();
        return view('admin.showusers', compact('users'));
    }

    //add user
    public function addUser()
    {
        
        return view('admin.adduser');
    }

    //store user
    public function storeUser(Request $request)
    {
        // validate the form
        request()->validate([
            'first_name' => 'required',
            'middle_name' => 'required',
            'last_name' => 'required',
            'username' => 'required',
            'password' => 'required',
        ]);
        // store the user
        User::create([
            'first_name' => $request->first_name,
            'middle_name' => $request->middle_name,
            'last_name' => $request->last_name,
            'username' => $request->username,
            'password' => Hash::make($request->password),
        ]);
        // redirect to showusers
        return redirect('/admin/showusers')->with("success","Added!");

    }


    //edit user
    public function editUser(User $user)
    {
        $users = User::all();
        return view('admin.edituser', compact('user'));
    }

    //update user
    public function updateUser(Request $request, User $user)
    {
        request()->validate([
            'first_name' => 'required',
            'middle_name' => 'required',
            'last_name' => 'required',
            'username' => 'required',
            'password' => 'required',
        ]);
        $user->update(array_merge($request->only('first_name', 'middle_name', 'last_name', 'username'), ['password' => Hash::make($request->password)]
        ));

        return redirect('/admin/showusers')->with("success","Updated!");
    }

    //delete user
    public function deleteUser(User $user)
    {
        $user->delete();

        return redirect()->back()->with("error","Deleted!");
    }


}
