@extends('layouts.master')
@section('content')
	<div class="card">
		<div class="card-header">
			<h5>Users</h5>
		</div>
		@if (session('error'))
	        <div class="alert alert-danger">
	            {{ session('error') }}
	        </div>
	    @endif
        @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
		<div class="card-body">
			<a href = '/admin/adduser' class="btn btn-primary">Add User</a>
			<hr>
			<table class="table table-bordered table-dark">
				<thead>
					<tr>
						<th>Full Name</th>
						<th>Username</th>
						<th>Created</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					@foreach($users as $user)
						<tr>
							<td>{{ $user->name }}</td>
							<td>{{ $user->username }}</td>
							<td>{{ $user->created_at }}</td>
							<td>
								<a href="/admin/showusers/{{ $user->id }}/edituser"><button class="btn btn-primary">Edit</button></a>
								<a href="/admin/showusers/{{ $user->id }}/deleteuser"><button class="btn btn-danger">Delete</button></a>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
          $(".alert").delay(2000).slideUp(300);
    });
    </script>
@endsection