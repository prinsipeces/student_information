@extends('layouts.master')
@section('content')
	<div class="card">
		<div class="card-header">
			<h5>Edit User</h5>
		</div>
		<div class="card-body">
			<form action="/admin/showusers/{{ $user->id }}/updateuser" method="POST">
				@csrf
				<div class="row">
					

					<div class="col-sm-4">
						<div class="form-group">
							<label for="first_name">First Name:</label>
							<input type="text" name="first_name" id="first_name" value="{{ $user->first_name }}" class='form-control' required>
						</div>
					</div>
					
					<div class="col-sm-4">
						<div class="form-group">
							<label for="middle_name">Middle Name:</label>
							<input type="text" name="middle_name" id="middle_name" value="{{ $user->middle_name }}" class='form-control' required>
						</div>
					</div>

					<div class="col-sm-4">
						<div class="form-group">
							<label for="last_name">Last Name:</label>
							<input type="text" name="last_name" id="last_name" value="{{ $user->last_name }}" class='form-control' required>
						</div>
					</div>

					<div class="d-flex col-sm-4">
						<div class="form-group">
							<label for="username">Username:</label>
							<input type="text" name="username" id="username" value="{{ $user->username }}" class='form-control' required>
						</div>
					</div>
					
					<div class="d-flex col-sm-4">
						<div class="form-group">
							<label for="password">Password:</label>
							<input type="text" name="password" id="password" placeholder="Password" class='form-control' required>
							<i style="margin-left: 90%;cursor: pointer;transform: translate(0,-180%);" class="far fa-eye" id="togglePassword"></i>
						</div>
					</div>


				</div>
				<div class="row">
					<div class="col-sm-12 d-flex justify-content-end">
						<button class="btn btn-primary">Save Changes</button>
					</div>
				</div>
			</form>			
		</div>
	</div>

<script>
	const togglePassword = document.querySelector('#togglePassword');
	const password = document.querySelector('#password');

	togglePassword.addEventListener('click', function (e) {
	    // toggle the type attribute
	    const type = password.getAttribute('type') === 'text' ? 'password' : 'text';
	    password.setAttribute('type', type);
	    // toggle the eye slash icon
	    this.classList.toggle('fa-eye-slash');
	});
 </script>
@endsection
