@extends('layouts.master')
@section('content')
	<div class="card">
		<div class="card-header">
			<h5>Students</h5>
		</div>
		@if (session('error'))
	        <div class="alert alert-danger">
	            {{ session('error') }}
	        </div>
	    @endif
        @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
		<div class="card-body">
			<a href = '/admin/create' class="btn btn-primary">Add Student</a>
			<hr>
			<table class="table table-bordered table-responsive">
				<thead>
					<tr>
						<th>Full Name</th>
						<th>Student ID No.</th>
						<th>Course</th>
						<th>Gender</th>
						<th>Email Address</th>
						<th>Phone No.</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					@foreach($students as $student)
						<tr>
							<td>
								<p>{{ $student->fullname }}</p>
							</td>
							<td>
								<p>{{ $student->student_id_number }}</p>
							</td>
							<td>
								 {{ $student->course_categories['course_name'] }}
							</td>
							<td>{{ $student->gender }}</td>
							<td>{{ $student->email_address }}</td>
							<td>{{ $student->phone_number }}</td>
							<td>
								<a style="text-decoration: none;" href="/admin/{{ $student->id }}/edit"><button class="btn btn-primary btn-block">Edit</button></a>
								<a style="text-decoration: none;" href="/admin/{{ $student->id }}/delete"><button class="btn btn-danger btn-block">Delete</button></a>

							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
          $(".alert").delay(2000).slideUp(300);
    });
    </script>
@endsection