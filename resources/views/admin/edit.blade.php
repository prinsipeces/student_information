@extends('layouts.master')
@section('content')
	<div class="card">
		<div class="card-header">
			<h5>Edit Student</h5>
		</div>
		<div class="card-body">
			<form action="/admin/{{ $student->id }}/update" method="POST">
				@csrf
				<div class="row">
					<div class="col-sm-4">
						<div class="form-group">
							<label for="first_name">First Name:</label>
							<input type="text" name="first_name" id="first_name" class='form-control' value="{{ $student->first_name }}" required>
						</div>
					</div>
					
					<div class="col-sm-4">
						<div class="form-group">
							<label for="middle_name">Middle Name:</label>
							<input type="text" name="middle_name" id="middle_name" value="{{ $student->middle_name }}" class='form-control' required>
						</div>
					</div>

					<div class="col-sm-4">
						<div class="form-group">
							<label for="last_name">Last Name:</label>
							<input type="text" name="last_name" id="last_name" value="{{ $student->last_name }}" class='form-control' required>
						</div>
					</div>

					<div class="col-sm-4">
						<div class="form-group">
							<label for="gender">Gender:</label>
							<select name="gender" id="gender" class='form-control' required>
								<option></option>
								<option value="Male">Male</option>
								<option value="Female">Female</option>
							</select>
						</div>
					</div>

					<div class="col-sm-4">
						<div class="form-group">
							<label for="email_address">Email Address:</label>
							<input type="text" name="email_address" id="email_address" value="{{ $student->email_address }}" class='form-control' required>
						</div>
					</div>

					<div class="col-sm-4">
						<div class="form-group">
							<label for="phone_number">Phone Number:</label>
							<input type="text" name="phone_number" id="phone_number" value="{{ $student->phone_number }}" class='form-control' required>
						</div>
					</div>

					<div class="col-sm-12">
						<div class="form-group">
							<label for="student_id_number">Student ID No.:</label>
							<input type="text" name="student_id_number" id="student_id_number" value="{{ $student->student_id_number }}" class='form-control' required>
						</div>
					</div>

					<div class="col-sm-12">
						<div class="form-group">
							<label for="course">Course:</label>
							<select name="course_id" id="course" class='form-control' required>
								<option></option>
								@foreach($course_categories as $category)
								<option value="{{$category->id}}">{{$category->course_name}}</option>
								@endforeach
							</select>
						</div>
					</div>

				</div>
				<div class="row">
					<div class="col-sm-12 d-flex justify-content-end">
						<button class="btn btn-primary">Save Changes</button>
					</div>
				</div>
			</form>			
		</div>
	</div>
@endsection
